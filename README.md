# GitLab Maven Template Refactor

The GitLab Maven Template is showing its age. It builds against Maven 3.3.x and Java 8. It also does not take full advantage of GitLab's CI/CD syntax changes.

## Fork

The GitLab project is [forked here](https://gitlab.com/timothy.stone/gitlab).

## Merge Request

The GitLab Merge request is [here](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98329).

## Changes

1. Leverage YAML Block with newline chomping, e.g., `FOO: >-`. Per §8.1.1.2 Block Chomping Indicator of the YAML specification:

  > Chomping controls how final *line breaks* and trailing *empty lines* are interpreted. 

  This makes the `MAVEN_OPTS` and `MAVEN_CLI_OPTS` readable and approachable.

2. Remove the use of `-Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener` in favor of Maven's `--no-transfer-progress` option. Available since Maven 3.6.
3. Upgrade the Maven `image` use Maven 3.8.7 (latest) on Java 8 LTS.
4. Use `extends`, a GitLab feature, over YAML Anchors.
5. ~~Use `rules` in favor of `only` and `except`. Using `rules` is the preferred GitLab feature.~~





